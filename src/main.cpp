#if _WIN32 || __linux__
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#endif

#if _WIN32
// #include
// "C:\\dev\\vcpkg\\packages\\gtest_x86-windows\\include\\gtest\\gtest.h" //
// error in import encountered [!?]
#include "C:\\dev\\vcpkg\\packages\\imgui_x86-windows\\include\\imgui.h"
#include "C:\\dev\\vcpkg\\packages\\sdl2_x86-windows\\include\\SDL2\\sdl.h"
#endif

#ifdef __linux__
#include <SDL2/SDL.h>
#include <imgui.h>
#endif

#include "../inc/coverage_testassert_module.h"
#include "../inc/current_process_modal_registry.h"

#define WINDOW_SCREEN_SIZE_WIDTH 600
#define WINDOW_SCREEN_SIZE_HEIGHT 480
#define WINDOW_APPLICATION_TITLE "Glare!"
// FPS is defined as so to be base-10:
#define FPS_SET_TARGET 60
// FPS target is 10, but -2 for 8 to reflect spritesheet animation indices
// +2 additional frames for collision and invincibility

class CommandContext {
  CommandContext *self_context;

public:
  int destination, source, user;
  size_t size;
}; // CommandContext and IntermediateCommand cannot be of the same breath
// These cannot intertwine with the IntermediateCommand because of the
// inheritance
class IntermediateCommand {
public: // member data;
  CommandContext *command_context;
  // member functions
  virtual void execute(); // this will be the most-derived method at runtime
};                        // Singleton's Strategy design pattern

class AttachAttackToUsage : public IntermediateCommand {
  AttachAttackToUsage *self_attachment;

public:
  AttachAttackToUsage(AttachAttackToUsage *self_attachment) {
    this->self_attachment = self_attachment;
  } // first we init
  // then we de-init
  ~AttachAttackToUsage() {
    std::free(this->self_attachment);
    this->self_attachment = std::nullptr_t();
    delete &self_attachment;
  }
};

class AttachDelayToUsage : public IntermediateCommand {
  AttachDelayToUsage *self_attachment;

public:
  AttachDelayToUsage(AttachDelayToUsage *self_attachment) {
    this->self_attachment = self_attachment;
  } // first we init
  // then we de-init
  ~AttachDelayToUsage() {
    std::free(this->self_attachment);
    this->self_attachment = std::nullptr_t();
    delete &self_attachment;
  }
};

class AttachSustainToUsage : public IntermediateCommand {
  AttachSustainToUsage *self_attachment;

public:
  AttachSustainToUsage(AttachSustainToUsage *self_attachment) {
    this->self_attachment = self_attachment;
  } // first we init
  // then we de-init
  ~AttachSustainToUsage() {
    std::free(this->self_attachment);
    this->self_attachment = std::nullptr_t();
    delete &self_attachment;
  }
};

class AttachReleaseToUsage : public IntermediateCommand {
  AttachReleaseToUsage *self_attachment;

public:
  AttachReleaseToUsage(AttachReleaseToUsage *self_attachment) {
    this->self_attachment = self_attachment;
  } // first we init
  // then we de-init
  ~AttachReleaseToUsage() {
    std::free(this->self_attachment);
    this->self_attachment = std::nullptr_t();
    delete &self_attachment;
  }
};

class AdditionHandler {
public:
  AttachAttackToUsage *attack_attachment;
  AttachDelayToUsage *delay_attachment;
  AttachSustainToUsage *sustain_attachment;
  AttachReleaseToUsage *release_attachment;

  int handle_modal_of_execution();
}; // Strategy design pattern for Prototype models

int attach_media_layer_window(SDL_Window *window, SDL_Surface *screen_surface) {
  window = std::nullptr_t();
  screen_surface = SDL_GetWindowSurface(window);

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL could not initialize the Window and Screen:\n\t%s",
           SDL_GetError());
  }

  SDL_FillRect(screen_surface, std::nullptr_t(),
               SDL_MapRGBA(screen_surface->format, 0x1B, 0x1B, 0x1B, 0xAA));

  SDL_UpdateWindowSurface(window);

  SDL_Event event;
  bool is_finished = false;
  while (is_finished != true) {
    if (event.type == SDL_QUIT) {
      SDL_DestroyWindow(window);
      SDL_Quit();

      screen_surface = std::nullptr_t();
      std::free(screen_surface);
      delete screen_surface;

      window = std::nullptr_t();
      std::free(window);

      return 1;
    }
  }

  return 0;
}

int deinit_media_layer_window(SDL_Window *window, SDL_Surface *screen_surface) {
  SDL_DestroyWindow(window);

  screen_surface = std::nullptr_t();
  std::free(screen_surface);
  delete screen_surface;

  window = std::nullptr_t();
  std::free(window);

  return 1;
}

class HandleMatrix {
public:
  int x, y;      // coordinates as members
  int *matrix[]; // the actual matrix member
};

class CheckRegisterHexSum {
  enum ArmRegisters arm_register;
  enum CurrentProcessorModes current_processor_mode;

  union { // field that tells us what is being used in a union
    CommandContext command_context;           // get the hex from the stack
    IntermediateCommand intermediate_command; // no ptr-type, str8>stack!!
  };

public:
  int hexsum_calculable() {
    int ret_val =
        CurrentProcessorModes::USER_MODE | this->current_processor_mode;
    // XOR our member binary values
    return ret_val;
  }

  int alias_hex_sum_merge(int usage_index) { // get input from the user table
    // NOTE(Daniel): check if we are in Arm's: USER MODE
    if (this->current_processor_mode != CurrentProcessorModes::USER_MODE) {
      return 1;
    }

    HandleMatrix *matrix; // init in heap-space with anonymous indices

    for (int i = 0;; i++)
      for (int j = 0;; j++) {
        matrix->matrix[i][j] = usage_index;

        switch (arm_register) {
        default:
          if (arm_register ^ matrix->matrix[i][j]) {
            this->command_context.destination =
                intermediate_command.command_context->destination = usage_index;
          }
        }
      }

    std::free(matrix);
    delete matrix;

    return 0;
  }
}; // merge these two strategies with aliasing

int AdditionHandler::handle_modal_of_execution() {
  SDL_LoadBMP("../assets/sprites/spritesheet.png");
  bool is_successful = true;

  if (SDL_LoadBMP("../assets/sprites/spritesheet.png") == NULL) {
    printf("Unable to load the image warranted!? %s", SDL_GetError());
    is_successful = false;

    SDL_FreeSurface(SDL_LoadBMP("../assets/sprites/spritesheet.png"));
    SDL_LoadBMP(NULL);
  }

  return is_successful;
}

int main(int argc, char *argv[]) {
  int ret_val = 0;

  enum ArmRegisters arm_registry;
  CommandContext *command_context;
  AdditionHandler *input_handler;

  switch (arm_registry) {
  default:
    enum ArmRegisters arm_usage = arm_registry;
    // now withhold the LHS value of a register for an IR module pass
    command_context->source = arm_registry;
  }

  SDL_Window *window;
  SDL_Surface *screen_surface, *image_surface;

  attach_media_layer_window(window, screen_surface);

  ImGui::Begin("PPU Recompiler: Coverage", std::nullptr_t(),
               ImGuiWindowFlags_MenuBar);
  if (ImGui::BeginMenuBar()) {
    if (ImGui::BeginMenu("File")) {
      if (ImGui::MenuItem("Open..", "Ctrl+O")) { /* Do stuff */
      }
      if (ImGui::MenuItem("Save", "Ctrl+S")) { /* Do stuff */
      }
      if (ImGui::MenuItem("Close", "Ctrl+W")) {
        ret_val = 1;
      }
      ImGui::EndMenu();
    }
    ImGui::EndMenuBar();
  }

  AdditionHandler *addition_handle;
  addition_handle->handle_modal_of_execution();

  SDL_BlitSurface(image_surface, NULL, screen_surface, NULL);
  // Display contents in a scrolling region
  ImGui::TextColored(ImVec4(1, 1, 0, 1), "PPU Recompiler Log");
  ImGui::BeginChild("Scrolling");
  for (int n = 0; n < 10; n++)
    ImGui::Text("Argument Counter:\t%d\nExpected:\t%08b\nActual:\t%08b", argc,
                command_context->source, command_context->destination);
  ImGui::EndChild();
  ImGui::End();

  deinit_media_layer_window(window, screen_surface);
  deinit_media_layer_window(NULL, image_surface);

  addition_handle = std::nullptr_t();
  std::free(addition_handle);
  delete addition_handle;

  SDL_Quit();

  return ret_val;
}
