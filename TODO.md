##### TODO(Daniel):

1. PPU into SPU

- Splice Basic Blocks (SPU)
  : alas determined (by PPU)

- Achieve solution
  : by re-using 'Known Instructions'

2. Interpret Block

- Recompile Program
- Cache with an 'Attache'

3. Static cross-compile Library

- Tracer profile (with GUI)
- Means to manipulate monitored blocks
  : Using a table editor[?]
