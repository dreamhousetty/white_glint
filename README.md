# white_glint

Trying to create an attachable profile-and-tracer for hardware emulation software (mostly ARM and x86 architectures).

##### TODO(Daniel):

1. PPU into SPU

- Splice Basic Blocks (SPU) : alas determined (by PPU)
- Achieve solution : by re-using 'Known Instructions'

2. Interpret Basic Block(s)

- Recompile Program
- Cache with an 'Attache'

3. Build LLVM IR Pass

- LHS against RHS comparator(s)
- Store Load and Store "Get Element Pointer" (GEP)
- C Memory Model/Harvard style cache model
- ARM7 Instruction Register (32-bit)

4. Static cross-compile Library

- Tracer profile (with GUI)
- Means to manipulate monitored blocks : Using a table editor \[?]

##### _In Detail:_

1. Dynamically specialized instructions

- PPU (Static) Interpreter
- PPU Method Just-in-Time Interpreter over a Static Interpreter, preferably

2. RTL code instructions
3. Hybrid stack/RTL instructions
4. Type-specialized instructions
5. Lazy basic block versioning
6. Profile-based specialization
7. Specialized iterator instructions
8. Dynamic flow of specialized instructions
