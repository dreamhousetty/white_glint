[requires]
imgui/1.88
llvm-core/13.0.0
sdl/2.26.0

[generators]
PkgConfigDeps
MesonToolchain
