#if _WIN32 || __linux__
#include <cassert>
#include <cstdio>
#include <cstdlib>
#endif

#if _WIN32
#include <C:\\llvm\\llvm\\include\\llvm\\IR\\Function.h>
#include <C:\\llvm\\llvm\\include\\llvm\\IR\\IRBuilder.h>
#include <C:\\llvm\\llvm\\include\\llvm\\IR\\LegacyPassManager.h>
#include <C:\\llvm\\llvm\\include\\llvm\\Pass.h>
#endif

#if __linux__
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#endif

typedef struct CData {
  void *destination, *source, *table;
  size_t size;
} CData;

using namespace llvm;

Value *GetDataOffset(size_t size);
Value *GetDataPtrOffset(void *data);
void OntoTheNextOneC();
// Anonymous namespaces are to C++ what the “static” keyword is to C (at global
// scope).
namespace /*Anonymous Namespace*/ {
class PPUFunctionPass : public FunctionPass {
public:
  static char ID;
  PPUFunctionPass() : FunctionPass(ID) {}

  virtual bool runOnFunction(Function &fn) {
    LLVMContext &context = fn.getContext();
    // std::vector<Type *> arguments = {Type::getInt8Ty(context)}; // get the
    // %0x8d of the top 1/2 register
    errs() << "PPU Pass:\t";
    errs().write_escaped(fn.getName()) << "\n";
    // TODO(Daniel): create a safe SPU Block first which can page into slices!
    for (auto &B : fn) {
      errs() << "Block:\n";
      B.print(errs());

      for (auto &I : B) {
        errs() << "Instructions:\n";
        I.print(errs(), true);
        errs() << "\n";

        if (auto *ops = dyn_cast<BinaryOperator>(&I)) {
          IRBuilder<> builder(ops);
          // CInput inputs; // Could be input. Could be output
          CData data;

          Value *offset = GetDataOffset(data.size);
          Value *buffer = GetDataPtrOffset(data.destination);
          void *c_type = std::memset(buffer, data.size, sizeof(Type *));

          Value *gep = builder.CreateGEP((Type *)c_type, buffer, offset);
          builder.CreateStore(offset, gep);
          OntoTheNextOneC();
        }
      }
    }

    return false;
  }
};
} // Anonymous Namespace

char PPUFunctionPass::ID = 0;
static RegisterPass<PPUFunctionPass>
    TestBeforeIRPassForPPU("Test...", "...PPU Pass Test",
                           false /*Looks at CFG*/, false /*Analysis Pass*/);
